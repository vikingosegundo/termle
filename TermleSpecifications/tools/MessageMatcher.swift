//
//  MessageMatcher.swift
//  TermleSpecifications
//
//  Created by vikingosegundo on 16/01/2022.
//

@testable import Termle
import TermleModel

func successfullyLoadedWordsMessage(_ m:Message?) -> Bool { if case .termle(.loading(.vocabulary(length:_,from:_, .succeeded   )))   = m { return true}; return false }
func failedLoadedWordsMessage      (_ m:Message?) -> Bool { if case .termle(.loading(.vocabulary(length:_,from:_, .failed(_)   )))   = m { return true}; return false }
func offeringWordSucceededMessage  (_ m:Message?) -> Bool { if case .termle(.offering(.word     (_,               .succeeded(_))))   = m { return true}; return false }
func offeringWordFailedMessage     (_ m:Message?) -> Bool { if case .termle(.offering(.word     (_,               .faild(_)    )))   = m { return true}; return false }
