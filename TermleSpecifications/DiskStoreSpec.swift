//
//  DiskStoreSpec.swift
//  Bert's RadiosTests
//
//  Created by vikingosegundo on 12/01/2022.
//
import Foundation
import Quick
import Nimble
import TermleModel
@testable import Termle

class DiskStoreSpec:QuickSpec {
    override func spec() {
        var store:Store!; beforeEach { store = createDiskStore(pathInDocs:"DiskStoreSpec.json") }; afterEach { destroy(&store) }
        context("DiskStore") {
            context("creation") {
                it("with empty word list"   ) { expect(store.state().vocabulary ).to(beEmpty()) }
                it("with empty current word") { expect(store.state().currentWord).to(beNil  ()) }
                context("setting") {
                    beforeEach {
                        change(store,.set(.words(["12345", "23456"])))
                        change(store,.set(.current(word:"12345"))) }
                    it("new words"   ) { expect(store.state().vocabulary ).to(equal(["12345", "23456"])) }
                    it("current word") { expect(store.state().currentWord).to(equal( "12345"))           }
                    context("resetting") {
                        beforeEach { reset(store) }
                        it("with empty station list" ) { expect(store.state().vocabulary ).to(beEmpty()) }
                        it("with empty fvourite list") { expect(store.state().currentWord).to(beNil  ()) } } } }
            context("observing") {
                var wasUpdated = false
                beforeEach { store.updated { wasUpdated = true } }
                 afterEach {                 wasUpdated = false  }
                context("setting current word" ) { beforeEach { store.change(.set(.current(word: "23455"))) }; it("will inform listeners") { expect(wasUpdated).to(beTrue()) } }
                context("setting new words"    ) { beforeEach { store.change(.set(.words([])))              }; it("will inform listeners") { expect(wasUpdated).to(beTrue()) } }
            }
        }
    }
}
