//
//  GameState.swift
//  TermleModel
//
//  Created by Manuel Meyer on 24.01.22.
//

public struct Game: Codable {
    public enum State: Codable {
    case notStarted
    case ongoing([TermleCharacter], remainingTries:UInt)
    case won    ([TermleCharacter])
    case lost   ([TermleCharacter])
    }
    public enum Change {
        case  set(_Set); public enum _Set {
            case state(Game.State) }
    }
    public init() { self.init(.notStarted, []) }
    
    public let state: Game.State
    public let previous: [Game.State]
    public func alter(_ change:Change) -> Self {
        switch change {
        case let .set(.state(state)): return .init(state, !isNotStarted(self.state) ? previous + [self.state] : [])
        }
    }
    private init (_ state:Game.State,_ previous: [Game.State]) {
        self.state    = state
        self.previous = previous
    }
}

fileprivate func isNotStarted(_ state:Game.State) -> Bool {
    switch state {
    case .notStarted: return true
    default         : return false
    }
}
