//
//  ViewStateSpec.swift
//  TermleSpecifications
//
//  Created by vikingosegundo on 16/01/2022.
//

import Foundation
import Quick
import Nimble
import TermleModel

@testable import Termle

class ViewStateSpec:QuickSpec {
    override func spec() {
        var store:Store!;         beforeEach { store     = createDiskStore(pathInDocs:"DiskStoreSpec.json") }; afterEach { destroy(&store) }
        var viewState:ViewState!; beforeEach { viewState = ViewState(store:store)                           }; afterEach { viewState = nil }
        describe("ViewState") {
            beforeEach { store.change(.set(.words(["abcde", "bcdef"]))) }
            context("setting words") {
                it("doesnt change current word") { expect(viewState.model.currentWord).to(equal("")) }
            }
            context("setting current word") {
                beforeEach { store.change(.set(.current(word:"abcde"))) }
                it("does change current word") { expect(viewState.model.currentWord).toEventually(equal("abcde")) }
                context("setting current word") {
                    beforeEach { store.change(.set(.current(word:"cdefg"))) }
                    it("does change current word") { expect(viewState.model.currentWord).toEventually(equal("cdefg")) }
                }
            }
        }
    }
}
