//
//  AppDomainSpecifications.swift
//  AppDomainSpecifications
//
//  Created by vikingosegundo on 16/01/2022.
//

import Quick
import Nimble
import Foundation
import TermleModel
@testable import Termle
import XCTest

class AppDomainSpecifications: QuickSpec {
    override func spec() {
        var appDomain: Input!
        var store    : Store!;         beforeEach { store     = createDiskStore(pathInDocs:"AppDomainSpecifications.json")                                                                           }; afterEach { destroy(&store)                }
        var messages : [Message] = []; beforeEach { appDomain = createAppDomain(store: store, bundle:Bundle(identifier:"com.vs.TermleSpecifications")!, receivers:[]) { messages = messages + [$0] } }; afterEach { appDomain = nil; messages = [] }
        var viewState: ViewState!;     beforeEach { viewState = ViewState(store: store)                                                                                                              }; afterEach { viewState = nil                }
        describe("Termle App") {
            context("loading data") {
                context("from existing file") {
                    beforeEach { appDomain(.termle(.load(.vocabulary(length:5, from:.path("words.txt"))))) }
                    it("receives successfully loaded message"   ) { expect(successfullyLoadedWordsMessage(messages.last)).to(beTrue())                                                                                 }
                    it("saves loaded words in store"            ) { expect(store.state().vocabulary                     ).to(equal(["solar","title","notes","shire","silly","super","soare","proxy","notee","robot"])) }
                    it("doenst change viewState currentWord"    ) { expect(viewState.model.currentWord                  ).to(equal(""))                                                                                }
                    it("saves loaded words in store"            ) { expect(store.state().wordLength                     ).to(equal(5))                                                                                 }
                    it("will expose history via viewState model") { expect(viewState.model.previous                     ).to(haveCount(0))                                                                             }
                    context("remaining tries") {
                        it("in viewstate"   ) { expect(viewState.model.remainingTries).toEventually(equal(6)) }
                        it("in stores"      ) { expect(store.state().remainingTries  ).to          (equal(6)) } } }
                context("from non-existing file") {
                    beforeEach { appDomain(.termle(.load(.vocabulary(length:5, from:.path("nowords.txt"))))) }
                    it("receives failed loading message"    ) { expect(failedLoadedWordsMessage(messages.last)).to(beTrue() ) }
                    it("saves no words in store"            ) { expect(store.state().vocabulary               ).to(equal([])) }
                    it("doesnt change viewState currentWord") { expect(viewState.model.currentWord            ).to(equal("")) }
                    context("remaining tries") {
                        it("in viewstate"   ) { expect(viewState.model.remainingTries   ).toEventually(equal(6)) }
                        it("in stores"      ) { expect(store.state().remainingTries     ).to          (equal(6)) } } } }
            context("offering") {
                beforeEach { appDomain(.termle(.load(.vocabulary(length:5, from:.path("words.txt"))))) }
                beforeEach { store.change(.set(.target(word:"silly")))                                 }
                context("valid word") {
                    beforeEach { appDomain(.termle(.offer(.word("solar")))) }
                    it("will be available in store"                  ) { expect(store.state().currentWord                  ).to          (equal("solar")) }
                    it("will trigger offering word succeeded message") { expect(offeringWordSucceededMessage(messages.last)).to          (beTrue()      ) }
                    it("does change viewState currentWord"           ) { expect(viewState.model.currentWord                ).toEventually(equal("solar")) }
                    it("will contain no previous state"              ) { expect(store.state().game.previous                ).to          (beEmpty())      }
                    it("will expose history via viewState model"     ) { expect(viewState.model.previous                   ).to          (haveCount(0))   }
                    context("remaining tries") {
                        it("in viewstate"   ) { expect(viewState.model.remainingTries).toEventually(equal(5)) }
                        it("in stores"      ) { expect(store.state().remainingTries  ).to          (equal(5)) } }
                    context("offering another valid word"){
                        beforeEach { appDomain(.termle(.offer(.word("title")))) }
                        it("will be available in store"                             ) { expect(store.state().currentWord                               ).to(equal("title")  )                                                                                   }
                        it("will move the previous current word into previous words") { expect(store.state().previousWords                             ).to(equal(["solar"]))                                                                                   }
                        it("will contain 1 previous state"                          ) { expect(store.state().game.previous                             ).to(haveCount(1))                                                                                       }
                        it("will contain 1 previous state, 1st"                     ) { expect(previousIsOngoingWithChars(at:0, for:store.state().game)).to(equal([.correctSpot("s"),.notPresent ("o"),.correctSpot("l"),.notPresent ("a"),.notPresent("r") ])) }
                        it("game not be lost yet with"                              ) { expect(gameIsOngoingWithChars(store.state().game)              ).to(equal([.notPresent ("t"),.correctSpot("i"),.notPresent ("t"),.correctSpot("l"),.notPresent("e") ])) }
                        it("will expose history via viewState model"                ) { expect(viewState.model.previous                                ).toEventually(haveCount(1))                                                                             }
                        context("remaining tries") {
                            it("in viewstate"   ) { expect(viewState.model.remainingTries).toEventually(equal(4)) }
                            it("in stores"      ) { expect(store.state().remainingTries  ).to          (equal(4)) } }
                        context("offering another valid word"){
                            beforeEach { appDomain(.termle(.offer(.word("soare")))) }
                            it("will be available in store"                             ) { expect(store.state().currentWord                               ).to(equal("soare"))                                                                                     }
                            it("will move the previous current word into previous words") { expect(store.state().previousWords                             ).to(equal(["solar","title"]))                                                                           }
                            it("will not be won yet"                                    ) { expect(gameWasWon (store.state().game)                         ).to(beFalse())                                                                                          }
                            it("will not be lost yet"                                   ) { expect(gameWasLost(store.state().game)                         ).to(beFalse())                                                                                          }
                            it("will contain 2 previous states"                         ) { expect(store.state().game.previous                             ).to(haveCount(2))                                                                                       }
                            it("will expose history via viewState model"                ) { expect(viewState.model.previous                                ).toEventually(haveCount(2))                                                                             }
                            it("will contain 2 previous states, 1st"                    ) { expect(previousIsOngoingWithChars(at:0, for:store.state().game)).to(equal([.correctSpot("s"),.notPresent ("o"),.correctSpot("l"),.notPresent ("a"),.notPresent("r") ])) }
                            it("will contain 2 previous states, 2nd"                    ) { expect(previousIsOngoingWithChars(at:1, for:store.state().game)).to(equal([.notPresent ("t"),.correctSpot("i"),.notPresent ("t"),.correctSpot("l"),.notPresent("e") ])) }
                            it("game not be lost yet with"                              ) { expect(gameIsOngoingWithChars(store.state().game)              ).to(equal([.correctSpot("s"),.notPresent ("o"),.notPresent ("a"),.notPresent ("r"),.notPresent("e") ])) }
                            context("remaining tries") {
                                it("in viewstate"   ) { expect(viewState.model.remainingTries).toEventually(equal(3)) }
                                it("in stores"      ) { expect(store.state().remainingTries  ).to          (equal(3)) } }
                            context("offering another valid word"){
                                beforeEach { appDomain(.termle(.offer(.word("shire")))) }
                                it("will be available in store"                             ) { expect(store.state().currentWord                               ).to(equal("shire"))                                                                                     }
                                it("will move the previous current word into previous words") { expect(store.state().previousWords                             ).to(equal(["solar","title","soare"]))                                                                   }
                                it("will not be won yet"                                    ) { expect(gameWasWon (store.state().game)                         ).to(beFalse())                                                                                          }
                                it("will not be lost yet"                                   ) { expect(gameWasLost(store.state().game)                         ).to(beFalse())                                                                                          }
                                it("will contain 3 previous states"                         ) { expect(store.state().game.previous                             ).to(haveCount(3))                                                                                       }
                                it("will expose history via viewState model"                ) { expect(viewState.model.previous                                ).toEventually(haveCount(3))                                                                             }
                                it("will contain 3 previous states, 1st"                    ) { expect(previousIsOngoingWithChars(at:0, for:store.state().game)).to(equal([.correctSpot("s"),.notPresent ("o"),.correctSpot("l"),.notPresent ("a"),.notPresent("r") ])) }
                                it("will contain 3 previous states, 2nd"                    ) { expect(previousIsOngoingWithChars(at:1, for:store.state().game)).to(equal([.notPresent ("t"),.correctSpot("i"),.notPresent ("t"),.correctSpot("l"),.notPresent("e") ])) }
                                it("will contain 3 previous states, 3rd"                    ) { expect(previousIsOngoingWithChars(at:2, for:store.state().game)).to(equal([.correctSpot("s"),.notPresent ("o"),.notPresent ("a"),.notPresent ("r"),.notPresent("e") ])) }
                                context("remaining tries") {
                                    it("in viewstate"   ) { expect(viewState.model.remainingTries).toEventually(equal(2)) }
                                    it("in stores"      ) { expect(store.state().remainingTries  ).to          (equal(2)) } }
                                context("offering another valid word"){
                                    beforeEach { appDomain(.termle(.offer(.word("super")))) }
                                    it("will be available in store"                             ) { expect(store.state().currentWord                               ).to(equal("super"))                                                                                       }
                                    it("will move the previous current word into previous words") { expect(store.state().previousWords                             ).to(equal(["solar","title","soare","shire"]))                                                             }
                                    it("game is started"                                        ) { expect(gameNotStarted(store.state().game)                      ).to(beFalse())                                                                                            }
                                    it("game is ongoing"                                        ) { expect(gameIsOngoing (store.state().game)                      ).to(beTrue ())                                                                                            }
                                    it("game not be lost yet"                                   ) { expect(gameWasLost   (store.state().game)                      ).to(beFalse())                                                                                            }
                                    it("will contain 4 previous states"                         ) { expect(store.state().game.previous                             ).to(haveCount(4))                                                                                         }
                                    it("will expose history via viewState model"                ) { expect(viewState.model.previous                                ).toEventually(haveCount(4))                                                                               }
                                    it("will contain 4 previous states, 1st"                    ) { expect(previousIsOngoingWithChars(at:0, for:store.state().game)).to(equal([.correctSpot("s"),.notPresent ("o"),.correctSpot  ("l"),.notPresent ("a"),.notPresent("r") ])) }
                                    it("will contain 4 previous states, 2nd"                    ) { expect(previousIsOngoingWithChars(at:1, for:store.state().game)).to(equal([.notPresent ("t"),.correctSpot("i"),.notPresent   ("t"),.correctSpot("l"),.notPresent("e") ])) }
                                    it("will contain 4 previous states, 3rd"                    ) { expect(previousIsOngoingWithChars(at:2, for:store.state().game)).to(equal([.correctSpot("s"),.notPresent ("o"),.notPresent   ("a"),.notPresent ("r"),.notPresent("e") ])) }
                                    it("will contain 4 previous states, 4th"                    ) { expect(previousIsOngoingWithChars(at:3, for:store.state().game)).to(equal([.correctSpot("s"),.notPresent ("h"),.presentInWord("i"),.notPresent ("r"),.notPresent("e") ])) }
                                    it("game not be lost yet with"                              ) { expect(gameIsOngoingWithChars(store.state().game)              ).to(equal([.correctSpot("s"),.notPresent ("u"),.notPresent   ("p"),.notPresent ("e"),.notPresent("r") ])) }
                                    context("remaining tries") {
                                        it("in viewstate"   ) { expect(viewState.model.remainingTries).toEventually(equal(1)) }
                                        it("in stores"      ) { expect(store.state().remainingTries  ).to          (equal(1)) } }
                                    context("offering another valid word"){
                                        beforeEach { appDomain(.termle(.offer(.word("silly")))) }
                                        it("will be available in store"                             ) { expect(store.state().currentWord                               ).to(equal("silly"))                                                                                        }
                                        it("will move the previous current word into previous words") { expect(store.state().previousWords                             ).to(equal(["solar","title","soare","shire","super"]))                                                      }
                                        it("will be won"                                            ) { expect(gameWasWon(store.state().game)                          ).to(beTrue())                                                                                              }
                                        it("will contain 5 previous states"                         ) { expect(store.state().game.previous                             ).to(haveCount(5))                                                                                          }
                                        it("will expose history via viewState model"                ) { expect(viewState.model.previous                                ).toEventually(haveCount(5))                                                                                }
                                        it("will contain 5 previous states, 1st"                    ) { expect(previousIsOngoingWithChars(at:0, for:store.state().game)).to(equal([.correctSpot("s"),.notPresent ("o"),.correctSpot  ("l"),.notPresent ("a"),.notPresent ("r") ])) }
                                        it("will contain 5 previous states, 2nd"                    ) { expect(previousIsOngoingWithChars(at:1, for:store.state().game)).to(equal([.notPresent ("t"),.correctSpot("i"),.notPresent   ("t"),.correctSpot("l"),.notPresent ("e") ])) }
                                        it("will contain 5 previous states, 3rd"                    ) { expect(previousIsOngoingWithChars(at:2, for:store.state().game)).to(equal([.correctSpot("s"),.notPresent ("o"),.notPresent   ("a"),.notPresent ("r"),.notPresent ("e") ])) }
                                        it("will contain 5 previous states, 4th"                    ) { expect(previousIsOngoingWithChars(at:3, for:store.state().game)).to(equal([.correctSpot("s"),.notPresent ("h"),.presentInWord("i"),.notPresent ("r"),.notPresent ("e") ])) }
                                        it("will contain 5 previous states, 5th"                    ) { expect(previousIsOngoingWithChars(at:4, for:store.state().game)).to(equal([.correctSpot("s"),.notPresent ("u"),.notPresent   ("p"),.notPresent ("e"),.notPresent ("r") ])) }
                                        it("winning with"                                           ) { expect(gameWasWonWithChars(store.state().game)                 ).to(equal([.correctSpot("s"),.correctSpot("i"),.correctSpot  ("l"),.correctSpot("l"),.correctSpot("y") ])) }
                                        context("remaining tries") {
                                            it("in viewstate"   ) { expect(viewState.model.remainingTries).toEventually(equal(0)) }
                                            it("in stores"      ) { expect(store.state().remainingTries  ).to          (equal(0)) } }
                                        context("offering another valid word"){
                                            beforeEach { appDomain(.termle(.offer(.word("notes")))) }
                                            it("will not be available in store"                             ) { expect(store.state().currentWord                               ).to(equal("silly"))                                                                                        }
                                            it("will not move the previous current word into previous words") { expect(store.state().previousWords                             ).to(equal(["solar","title","soare","shire","super"]))                                                      }
                                            it("will be won"                                                ) { expect(gameWasWon(store.state().game)                          ).to(beTrue())                                                                                              }
                                            it("will contain 5 previous states"                             ) { expect(store.state().game.previous                             ).to(haveCount(5))                                                                                          }
                                            it("will expose history via viewState model"                    ) { expect(viewState.model.previous                                ).toEventually(haveCount(5))                                                                                }
                                            it("will contain 5 previous states, 1st"                        ) { expect(previousIsOngoingWithChars(at:0, for:store.state().game)).to(equal([.correctSpot("s"),.notPresent ("o"),.correctSpot  ("l"),.notPresent ("a"),.notPresent ("r") ])) }
                                            it("will contain 5 previous states, 2nd"                        ) { expect(previousIsOngoingWithChars(at:1, for:store.state().game)).to(equal([.notPresent ("t"),.correctSpot("i"),.notPresent   ("t"),.correctSpot("l"),.notPresent ("e") ])) }
                                            it("will contain 5 previous states, 3rd"                        ) { expect(previousIsOngoingWithChars(at:2, for:store.state().game)).to(equal([.correctSpot("s"),.notPresent ("o"),.notPresent   ("a"),.notPresent ("r"),.notPresent ("e") ])) }
                                            it("will contain 5 previous states, 4th"                        ) { expect(previousIsOngoingWithChars(at:3, for:store.state().game)).to(equal([.correctSpot("s"),.notPresent ("h"),.presentInWord("i"),.notPresent ("r"),.notPresent ("e") ])) }
                                            it("will contain 5 previous states, 5th"                        ) { expect(previousIsOngoingWithChars(at:4, for:store.state().game)).to(equal([.correctSpot("s"),.notPresent ("u"),.notPresent   ("p"),.notPresent ("e"),.notPresent ("r") ])) }
                                            context("remaining tries") {
                                                it("in viewstate") { expect(viewState.model.remainingTries   ).toEventually(equal(0)) }
                                                it("in stores"   ) { expect(store.state().remainingTries     ).to          (equal(0)) } } } }
                                    context("offering another valid word") {
                                        beforeEach { appDomain(.termle(.offer(.word("solar")))) }
                                        it("will be available in store"                             ) { expect(store.state().currentWord      ).to(equal("solar"))                                   }
                                        it("will move the previous current word into previous words") { expect(store.state().previousWords    ).to(equal(["solar","title","soare","shire","super"])) }
                                        it("will be lost"                                           ) { expect(gameWasLost(store.state().game)).to(beTrue())                                         }
                                        context("remaining tries") {
                                            it("in viewstate") { expect(viewState.model.remainingTries).toEventually(equal(0)) }
                                            it("in stores"   ) { expect(store.state().remainingTries  ).to          (equal(0)) } } } } } } } }
                context("invalid word") {
                    context("too long") {
                        beforeEach { appDomain(.termle(.offer(.word("solarsystem")))) }
                        it("will not be available in store"           ) { expect(store.state().currentWord               ).to(beNil  ()) }
                        it("will trigger offering word failed message") { expect(offeringWordFailedMessage(messages.last)).to(beTrue ()) }
                        it("doesnt change viewState currentWord"      ) { expect(viewState.model.currentWord             ).to(equal("")) }
                        it("will not be won yet"                      ) { expect(gameWasWon    (store.state().game)      ).to(beFalse()) }
                        it("game is not ongoing"                      ) { expect(gameIsOngoing (store.state().game)      ).to(beFalse()) }
                        it("will not be lost yet"                     ) { expect(gameWasLost   (store.state().game)      ).to(beFalse()) }
                        it("game is not started yet"                  ) { expect(gameNotStarted(store.state().game)      ).to(beTrue ()) }
                        context("remaining tries") {
                            it("in viewstate"   ) { expect(viewState.model.remainingTries).toEventually(equal(6)) }
                            it("in stores"      ) { expect(store.state().remainingTries  ).to          (equal(6)) } }
                        context("offering another valid word"){
                            beforeEach { appDomain(.termle(.offer(.word("notes")))) }
                            it("will not be available in store"                             ) { expect(store.state().currentWord                 ).to(equal("notes")) }
                            it("will not move the previous current word into previous words") { expect(store.state().previousWords               ).to(equal([]))      }
                            it("will not be won yet"                                        ) { expect(gameWasWon            (store.state().game)).to(beFalse())      }
                            it("game is ongoing"                                            ) { expect(gameIsOngoing         (store.state().game)).to(beTrue ())      }
                            it("will not be lost yet"                                       ) { expect(gameWasLost           (store.state().game)).to(beFalse())      }
                            it("game is started"                                            ) { expect(gameNotStarted        (store.state().game)).to(beFalse())      }
                            it("will not be won yet"                                        ) { expect(gameIsOngoingWithChars(store.state().game)).to(equal([
                                .notPresent   ("n"),
                                .notPresent   ("o"),
                                .notPresent   ("t"),
                                .notPresent   ("e"),
                                .presentInWord("s") ])) }
                            context("remaining tries") {
                                it("in viewstate"   ) { expect(viewState.model.remainingTries).toEventually(equal(5)) }
                                it("in stores"      ) { expect(store.state().remainingTries  ).to          (equal(5)) } } } }
                    context("not in list") {
                        beforeEach { appDomain(.termle(.offer(.word("12345")))) }
                        it("will not be available in store"           ) { expect(store.state().currentWord               ).to(beNil ())  }
                        it("will trigger offering word failed message") { expect(offeringWordFailedMessage(messages.last)).to(beTrue())  }
                        it("doesnt change viewState currentWord"      ) { expect(viewState.model.currentWord             ).to(equal("")) }
                        context("remaining tries") {
                            it("in viewstate"   ) { expect(viewState.model.remainingTries).toEventually(equal(6)) }
                            it("in stores"      ) { expect(store.state().remainingTries  ).to          (equal(6)) } } } } }
            context("evaluate") {
                beforeEach { appDomain(.termle(.load(.vocabulary(length:5, from:.path("words.txt"))))) }
                beforeEach { store.change(.set(.target(word:"notes")))                               }
                context("valid word") {
                    beforeEach { appDomain(.termle(.offer(.word("notee")))) }
                    it("game not finished yet with") { expect(gameIsOngoingWithChars(store.state().game)).to(equal([.correctSpot("n"),.correctSpot("o"),.correctSpot("t"),.correctSpot("e"),.notPresent("e") ])) }
                    context("valid word") {
                        beforeEach { appDomain(.termle(.offer(.word("notes")))) }
                        it("won game") { expect(gameWasWonWithChars(store.state().game)).to(equal([.correctSpot("n"),.correctSpot("o"),.correctSpot("t"),.correctSpot("e"),.correctSpot("s") ])) } } } }
        }
    }
}
private func gameWasWon                (          _ game:Game) -> Bool              { switch game.state { case     .won             : return true  default: return false } }
private func gameWasLost               (          _ game:Game) -> Bool              { switch game.state { case     .lost            : return true  default: return false } }
private func gameIsOngoing             (          _ game:Game) -> Bool              { switch game.state { case     .ongoing         : return true  default: return false } }
private func gameNotStarted            (          _ game:Game) -> Bool              { switch game.state { case     .notStarted      : return true  default: return false } }
private func gameWasWonWithChars       (          _ game:Game) -> [TermleCharacter] { switch game.state { case let .won(chars)      : return chars default: return []    } }
private func gameIsOngoingWithChars    (          _ game:Game) -> [TermleCharacter] { switch game.state { case let .ongoing(chars,_): return chars default: return []    } }
private func previousIsOngoingWithChars(at:Int, for game:Game) -> [TermleCharacter] {
    guard game.previous.count > at else { return [] }
    switch game.previous[at] {
    case let .ongoing(chars,_) : return chars
    default: return []
    }
}
