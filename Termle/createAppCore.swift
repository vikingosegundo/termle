//
//  createAppCore.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 11.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import Foundation
import TermleModel

typealias  Input = (Message) -> ()
typealias Output = (Message) -> ()

func createAppDomain(
    store      : Store,
    bundle     : Bundle,
    receivers  : [Input],
    rootHandler: @escaping Output) -> Input
{
    let features: [Input] = [
        createTermleFeature(store: store, bundle: bundle, output: rootHandler)
    ]
    return { msg in
        (receivers + features).forEach { $0(msg) }
    }
}
