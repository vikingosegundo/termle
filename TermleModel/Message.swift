//
//  Message.swift
//  Termle
//
//  Created by vikingosegundo on 15/01/2022.
//

import TermleTools

public enum Message {
    case termle(Termle) // Feature "Termle"
}

//MARK: - Feature "Termle"
extension Message {
    public enum Termle   {
        case load    (Load    ); public enum Load     { case vocabulary(length:UInt,from:From)          }
        case loading (Loading ); public enum Loading  { case vocabulary(length:UInt,from:From, Outcome) }
        case offer   (Offer   ); public enum Offer    { case word      (String)                         }
        case offering(Offering); public enum Offering {
            case word(String, Outcome); public enum Outcome {
                case succeeded(Game)
                case faild (Failed  );  public enum Failed {
                    case word(Reason);  public enum Reason {
                        case unkonw
                        case mismatchingSize(given:UInt, expected:UInt)
                    }
                }
            }
        }
    }
}

// generel DSL Elements
public enum Outcome { case succeeded, failed(Error?) }
public enum From    { case path(String)              }
