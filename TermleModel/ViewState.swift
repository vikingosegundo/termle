//
//  ViewState.swift
//  BrighterHue
//
//  Created by Manuel Meyer on 11.06.21.
//  Copyright © 2021 Manuel Meyer. All rights reserved.
//

import SwiftUI
import Combine



public final class ViewState: ObservableObject {
    @Published public var model: ViewModel = ViewModel()
    public init(store: Store) {
        store.updated { self.process(state(in: store)) }
        process(state(in: store))
    }
    public func process(_ appState: AppState) {
        DispatchQueue.main.async {
            self.model = self.model
                .alter(.by(.setting(.wordLength    (appState.wordLength                                        ))))
                .alter(.by(.setting(.currentTry    (UInt(appState.previousWords.count + 1)                     ))))
                .alter(.by(.setting(.numberOfTries (UInt(appState.previousWords.count) + appState.remainingTries))))
                .alter(.by(.setting(.remainingTries(appState.remainingTries                                     ))))
                .alter(.by(.setting(.currentWord   (appState.currentWord ?? ""                                 ))))
                .alter(.by(.adding (.previous      (history(from:appState)                                     ))))
        }
    }
}

private func history(from appState:AppState) -> [[ViewModel.Character]] {
    appState.game.previous.map {
        switch $0 {
        case .ongoing(let chars, remainingTries:_): return chars.map {
            switch $0 {
            case .correctSpot  (let s): return .correctSpot(s)
            case .notPresent   (let s): return .notPresent(s)
            case .presentInWord(let s): return .present(s) } }
        case .notStarted    : return []
        case .won(let chars): return chars.map {
            switch $0 {
            case .correctSpot  (let s): return .correctSpot(s)
            case .notPresent   (let s): return .notPresent(s)
            case .presentInWord(let s): return .present(s) } }
        case .lost(let chars): return chars.map {
                switch $0 {
                case .correctSpot  (let s): return .correctSpot(s)
                case .notPresent   (let s): return .notPresent(s)
                case .presentInWord(let s): return .present(s) } }
        }
    }
}
