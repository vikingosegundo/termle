//
//  TermleApp.swift
//  Termle
//
//  Created by vikingosegundo on 15/01/2022.
//

import SwiftUI
import TermleModel
import TermleUI

@main
final
class TermleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(viewState:viewState, rootHandler:rootHandler)
        }
    }
    
    private      let store      : Store           = createDiskStore()
    private lazy var viewState  : ViewState       = ViewState(store: store)
    private lazy var rootHandler: (Message) -> () = createAppDomain(
        store      : store,
        bundle     : Bundle.main,
        receivers  : [ ],
        rootHandler: { self.rootHandler($0) }
    )
}
