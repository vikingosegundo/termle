//
//  TermleCharacter.swift
//  TermleModel
//
//  Created by Manuel Meyer on 24.01.22.
//

public enum TermleCharacter: Codable, Equatable {
    case correctSpot  (String.Element)
    case presentInWord(String.Element)
    case notPresent   (String.Element)
}
