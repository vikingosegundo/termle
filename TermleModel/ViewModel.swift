//
//  ViewModel.swift
//  TermleModel
//
//  Created by vikingosegundo on 27/01/2022.
//

public struct ViewModel {
    public enum Character {
        case present(String.Element)
        case correctSpot(String.Element)
        case notPresent(String.Element)
    }
    public let numberOfTries : UInt
    public let currentTry    : UInt
    public let wordLength    : UInt
    public let remainingTries: UInt
    public let currentWord   : String
    public let previous      : [[ViewModel.Character]]
    public enum Change {
        case by(_By)
        public enum _By {
            case setting(_Setting); public enum _Setting {
            case numberOfTries (UInt)
            case remainingTries(UInt)
            case currentTry    (UInt)
            case wordLength    (UInt)
            case currentWord   (String) }
            case adding(_Adding); public enum _Adding {
                case previous([[ViewModel.Character]]) }
        }
    }
    public init() { self.init(6, 6, 0, 5, "", []) }
    public func alter(_ change:Change) -> Self {
        switch change {
        case .by(.setting(.numberOfTries (let numberOfTries ))): return .init(numberOfTries,remainingTries,currentTry,wordLength,currentWord,previous)
        case .by(.setting(.remainingTries(let remainingTries))): return .init(numberOfTries,remainingTries,currentTry,wordLength,currentWord,previous)
        case .by(.setting(.currentTry    (let currentTry    ))): return .init(numberOfTries,remainingTries,currentTry,wordLength,currentWord,previous)
        case .by(.setting(.wordLength    (let wordLength    ))): return .init(numberOfTries,remainingTries,currentTry,wordLength,currentWord,previous)
        case .by(.setting(.currentWord   (let currentWord   ))): return .init(numberOfTries,remainingTries,currentTry,wordLength,currentWord,previous)
        case .by(.adding (.previous      (let previous      ))): return .init(numberOfTries,remainingTries,currentTry,wordLength,currentWord,previous)
        }
    }
    private init(_ numberOfTries:UInt,_ remainingTries:UInt,_ currentTry:UInt,_ wordLength:UInt,_ currentWord:String,_ previous:[[ViewModel.Character]]) {
        self.numberOfTries  = numberOfTries
        self.remainingTries = remainingTries
        self.currentTry     = currentTry
        self.wordLength     = wordLength
        self.currentWord    = currentWord
        self.previous       = previous
    }
}
