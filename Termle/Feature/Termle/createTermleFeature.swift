//
//  createTermleFeature.swift
//  Termle
//
//  Created by vikingosegundo on 16/01/2022.
//

import Foundation
import TermleTools
import TermleModel

func createTermleFeature(store: Store, bundle:Bundle, output:@escaping Output) -> Input {
    let wordsloader =       WordsLoader(store:store, bundle:bundle, responder:handle(            output:output))
    let setter      = CurrentWordSetter(store:store,                responder:handle(store:store,output:output))
    func execute(command cmd:Message.Termle) {
        if case let .load(.vocabulary(length:l,from: path)) = cmd { wordsloader.request(.load(length:l,from:path)) }
        if case let .offer(.word(word))                     = cmd {      setter.request( .set(word:word))          }
    }
    return {
        if case .termle(let c) = $0 { execute(command:c) } // execute if termle feature is command's recipient
    }
}

private func handle(             output: @escaping Output) -> (      WordsLoader.Response) -> () { { process(            response:$0,output:output) } }
private func handle(store:Store, output: @escaping Output) -> (CurrentWordSetter.Response) -> () { { process(store:store,response:$0,output:output) } }

private func process(response: WordsLoader.Response, output: @escaping Output) {
    switch response {
    case let .loading(length:l,from:from,outcome): output(.termle(.loading(.vocabulary(length:l,from:from,outcome))))
    }
}
private func process(store:Store,response:CurrentWordSetter.Response, output: @escaping Output) {
    switch response {
    case let .setting(word: word, .succeeded):
        change(store,.decreaseTries);
        change(store, .set(.gameSate(
            ((store.state().remainingTries > 0) && word != store.state().targetWord)
            ? store.state().game.alter(.set(.state(.ongoing(calculateCharacters(string:word, target:store.state().targetWord), remainingTries: store.state().remainingTries))))
                : word == store.state().targetWord
                    ? store.state().game.alter(.set(.state(.won (calculateCharacters(string:word, target:store.state().targetWord)))))
                    : store.state().game.alter(.set(.state(.lost(calculateCharacters(string:word, target:store.state().targetWord)) ))))))
        output(.termle(.offering(.word(word,.succeeded(store.state().game)))))
    case let .setting(word:word,.failed(_)):
        output(.termle(.offering(.word(word,.faild(.word(.unkonw))))))
    }
}

func calculateCharacters(string:String, target:String) -> [TermleCharacter] {
    let stringElements = string.map {$0}
    let targetElements = target.map {$0}
    return zip(stringElements, targetElements).enumerated().map {
        return $1.0 == $1.1
            ? .correctSpot($1.0)
            : !target.contains($1.0)
                ? .notPresent($1.0)
                : String(string.prefix($0)).count(of: $1.0) >= String(target.prefix($0)).count(of: $1.0)
                    ? .notPresent($1.0)
                    : .presentInWord($1.0)
    }
}
