//
//  WordsLoader.swift
//  Termle
//
//  Created by vikingosegundo on 16/01/2022.
//
import Foundation
import TermleModel

struct WordsLoader: UseCase {
    enum Error: E { case unknown }
    enum Request { case load   (length:UInt,from:From         ) }
    enum Response{ case loading(length:UInt,from:From, Outcome) }

    init(store s:Store, bundle b:Bundle, responder r: @escaping (Response) -> ()) { store = s; bundle = b; respond = r }
    
    func request(_ request:Request) {
        func loadWordsIntoStore(from path:String,length:UInt) throws {
            store.change(.set(.words(try String(contentsOf:URL(fileURLWithPath:path))
                                        .components(separatedBy:.whitespacesAndNewlines)
                                        .compactMap { $0.count == length ? $0 : nil } )))
            respond(.loading(length:length,from:.path(path),.succeeded))
        }
        func bundlePath(for p:String) throws -> String {
            let segments = Array(p.split(separator:"."))
            guard
                segments.count > 1,
                let path = bundle.path(forResource:segments.prefix(segments.count - 1).joined(separator:"."), ofType:String(segments.last!))
            else {
                throw Error.unknown
            }
            return path
        }
        switch request {
        case let .load(l,.path(p)):
            do {
                try loadWordsIntoStore(from:bundlePath(for:p),length:l)
            } catch {
                respond(.loading(length:l, from:.path(p), .failed(Error.unknown)))
            }
        }
    }
    
    private let store  : Store
    private let respond: (Response) -> ()
    private let bundle : Bundle
    typealias RequestType  = Request
    typealias ResponseType = Response
}

fileprivate typealias E = Error
