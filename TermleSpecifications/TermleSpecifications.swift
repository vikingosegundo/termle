//
//  TermleSpecifications.swift
//  TermleSpecifications
//
//  Created by vikingosegundo on 16/01/2022.
//

import Quick
import Nimble
import Foundation
import TermleModel
@testable import Termle

class TermleSpecifications: QuickSpec {
    override func spec() {
        var feature: Input!
        var store   : Store!;         beforeEach { store   = createDiskStore(pathInDocs:"TermleSpecifications.json")                                                                  }; afterEach { destroy(&store)              }
        var messages: [Message] = []; beforeEach { feature = createTermleFeature(store:store, bundle: Bundle(identifier:"com.vs.TermleSpecifications")!) { messages = messages+[$0] } }; afterEach { feature = nil; messages = [] }
        describe("Termle Feature") {
            context("loading data") {
                context("from existing file") {
                    context("word length 5") {
                        beforeEach { feature(.termle(.load(.vocabulary(length:5, from:.path("words.txt"))))) }
                        it("receives successfully loaded message") { expect(successfullyLoadedWordsMessage(messages.last)).toEventually(beTrue())                                                                         }
                        it("saves loaded words in store"         ) { expect(store.state().vocabulary)                     .to(equal([
                            "solar","title","notes","shire","silly","super","soare","proxy","notee","robot"])) } }
                    context("word length 3") {
                        beforeEach { feature(.termle(.load(.vocabulary(length:3, from:.path("words.txt"))))) }
                        it("receives successfully loaded message") { expect(successfullyLoadedWordsMessage(messages.last)).toEventually(beTrue())                }
                        it("saves loaded words in store"         ) { expect(store.state().vocabulary)                     .toEventually(equal(["put", "yes"])) } } }
                context("from non-existing file") {
                    beforeEach { feature(.termle(.load(.vocabulary(length:5, from:.path("nowords.txt"))))) }
                    it("receives failed loading message") { expect(failedLoadedWordsMessage(messages.last)).toEventually(beTrue())  }
                    it("saves no words in store"        ) { expect(store.state().vocabulary)               .toEventually(equal([])) } } }
            context("offering") {
                beforeEach { feature(.termle(.load(.vocabulary(length:5, from:.path("words.txt"))))) }
                context("valid word") {
                    beforeEach { feature(.termle(.offer(.word("solar")))) }
                    it("will be available in store"                  ) { expect(store.state().currentWord                  ).to(equal("solar")) }
                    it("will trigger offering word succeeded message") { expect(offeringWordSucceededMessage(messages.last)).to(beTrue()      ) } }
                context("ivalid word") {
                    context("too long") {
                        beforeEach { feature(.termle(.offer(.word("solarsystem")))) }
                        it("will not be available in store"           ) { expect(store.state().currentWord               ).to(beNil ()) }
                        it("will trigger offering word failed message") { expect(offeringWordFailedMessage(messages.last)).to(beTrue()) } }
                    context("not in list") {
                        beforeEach { feature(.termle(.offer(.word("12345")))) }
                        it("will not be available in store"           ) { expect(store.state().currentWord               ).to(beNil ()) }
                        it("will trigger offering word failed message") { expect(offeringWordFailedMessage(messages.last)).to(beTrue()) } } } }
        }
    }
}
