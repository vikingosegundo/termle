//
//  State.swift
//  Bert's Radios
//
//  Created by vikingosegundo on 15/12/2021.
//

public struct AppState: Codable {
    public enum Change {
        case set(_Set); public enum _Set {
            case words  (    [String])
            case current(word:String )
            case  target(word:String )
            case gameSate(Game)
        }
        case add(_Add); public enum _Add {
            case word(String,to:Destination) }
        case decreaseTries
    }
    public enum Destination { case previousWords }
    public init () { self.init([], nil, "", [],.init().alter(.set(.state(.notStarted))), 6, 5) }
    public let remainingTries: UInt
    public let vocabulary   : [String]
    public let currentWord  : String?
    public let  targetWord  : String
    public let previousWords: [String]
    public let game    : Game
    public let wordLength   : UInt
    
    public func alter(_ changes:[Change]) -> Self { changes.reduce(self) { $0.alter($1) } }
}

private extension AppState {
    init(_ vocabulary   : [String] ,
         _ current      :  String? ,
         _  target      :  String  ,
         _ previousWords: [String] ,
         _ game         : Game,
         _ remainingTries: UInt     ,
         _ wordLength   : UInt     )
    {
        self.vocabulary    = vocabulary
        self.currentWord   = current
        self.targetWord    = target
        self.previousWords = previousWords
        self.game     = game
        self.remainingTries = remainingTries
        self.wordLength    = wordLength
    }
    func alter(_ change:Change) -> Self {
        switch change {
        case let .set(.words(vocabulary))           : return .init(vocabulary, currentWord                          ,targetWord,previousWords,          game, remainingTries    , wordLength)
        case let .set(.current(word:currentWord))   : return .init(vocabulary, currentWord != "" ? currentWord : nil,targetWord,previousWords,          game, remainingTries    , wordLength)
        case let .add(.word(word,to:.previousWords)): return .init(vocabulary, currentWord                          ,targetWord,previousWords + [word], game, remainingTries    , wordLength)
        case     .decreaseTries                     : return .init(vocabulary, currentWord                          ,targetWord,previousWords,          game, remainingTries - 1, wordLength)
        case let .set(.target(word:targetWord))     : return .init(vocabulary, currentWord                          ,targetWord,previousWords,          game, remainingTries    , wordLength)
        case let .set(.gameSate(gameState))         : return .init(vocabulary, currentWord                          ,targetWord,previousWords,          gameState, remainingTries    , wordLength)
        }
    }
}
