//
//  CurrentWordSetter.swift
//  Termle
//
//  Created by vikingosegundo on 16/01/2022.
//
import TermleModel

struct CurrentWordSetter: UseCase {
    enum Request  { case set    (word:String         ) }
    enum Response { case setting(word:String, Outcome) }
    
    init(store s: Store, responder r: @escaping (Response) -> ()) { store = s; respond = r }

    func request(_ req:Request) {
        func failedResponse(with word:String) { store.change(.set(.current(word:""))); respond(.setting(word: word, .failed(.none))) }
        func add(word:String) {
            if  ( store.state().currentWord != nil
               && store.state().remainingTries > 0) {
                change(store,.add(.word(store.state().currentWord!,to:.previousWords)))
            }
        }
        switch req {
        case let .set(word:word):
            guard
                word.count == store.state().wordLength,
                store.state().vocabulary.contains(word)
            else {
                failedResponse(with:word); return
            }
            add(word:word)
            switch (store.state().currentWord,
                    store.state().remainingTries) {
            case let (.some(w), 0):                                          respond(.setting(word:w, .failed(nil)))
            default               : change(store,.set(.current(word:word))); respond(.setting(word:word,.succeeded))
            }
        }
    }
    private let store  : Store
    private let respond: (Response) -> ()
    typealias RequestType = Request
    typealias ResponseType = Response
}
