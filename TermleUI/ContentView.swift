//
//  ContentView.swift
//  Termle
//
//  Created by vikingosegundo on 15/01/2022.
//

import SwiftUI
import TermleModel

public struct TermleRow: View {
    enum Data {
        case unused
        case input([Character])
        case output([TermleCharacter])
    }
    let data: Data
    public var body: some View {
        HStack {
            Text(characterFromData(data)[0]).frame(width:24).padding().border(colorFromData(data)[0],width:3)
            Text(characterFromData(data)[1]).frame(width:24).padding().border(colorFromData(data)[1],width:3)
            Text(characterFromData(data)[2]).frame(width:24).padding().border(colorFromData(data)[2],width:3)
            Text(characterFromData(data)[3]).frame(width:24).padding().border(colorFromData(data)[3],width:3)
            Text(characterFromData(data)[4]).frame(width:24).padding().border(colorFromData(data)[4],width:3)
        }
    }
}
func characterFromData(_ data:TermleRow.Data) -> [String] {
    switch data {
    case .unused:
        return "     ".map{ "\($0)" }
    case .input(let array):
        return array.reduce("") { $0 + "\($1)" }.map { "\($0)" }
    case .output(let array):
        return array.reduce("") {
            switch $1 {
            case .correctSpot  (let s): return $0 + "\(s)"
            case .presentInWord(let s): return $0 + "\(s)"
            case .notPresent   (let s): return $0 + "\(s)"
            }
        }.map { "\($0)" }
    }
}
func colorFromData(_ data:TermleRow.Data) -> [Color] {
    switch data {
    case .unused:
        return [.black,.black,.black,.black,.black]
    case .input:
        return [.black,.black,.black,.black,.black]
    case .output(let array):
        return array.reduce([]) {
            switch $1 {
            case .correctSpot  : return $0 + [.green ]
            case .presentInWord: return $0 + [.yellow]
            case .notPresent   : return $0 + [.gray  ]
            }
        }
    }
}
public struct TermleView: View {
    public var body: some View {
        ScrollView {
            TermleRow(data:.output([.presentInWord("T"),.presentInWord("I"), .notPresent("T"), .notPresent("L"), .notPresent("E")]))
            TermleRow(data:.output([   .notPresent("S"),  .correctSpot("O"), .notPresent("A"), .notPresent("R"), .notPresent("E")]))
            TermleRow(data:.output([  .correctSpot("P"),  .correctSpot("O"),.correctSpot("I"),.correctSpot("N"),.correctSpot("T")]))
            TermleRow(data:.input(["P","O","I","N","T"]))
            TermleRow(data:.unused)
            TermleRow(data:.unused)
        }
    }
}
public struct ContentView: View {
    public init(viewState: ViewState, rootHandler: @escaping (Message) -> ()) {
        self.viewState   = viewState
        self.rootHandler = rootHandler
    }
    public var body: some View {
        TermleView()
    }
    @ObservedObject
    private var viewState  : ViewState
    private let rootHandler: (Message) -> ()
}
